var webpack = require('webpack');
var path = require('path');

var BUILD_DIR = path.resolve(__dirname, 'public/js');
var APP_DIR = path.resolve(__dirname, 'src');

var config = {
	entry: APP_DIR + '/index.js',

	watch : true,

	watchOptions: {
		aggregateTimeout: 100
	},

	devtool: 'source-map',

	output: {
		path: BUILD_DIR,
		filename: 'bundle.js'
	},
	module : {
		loaders : [
			{
				test : /\.js$/,
				exclude: /node_modules/,
				loader : 'babel-loader',
				query: {
					presets: ['es2015', 'react'],
					plugins: ['transform-react-jsx', 'transform-runtime']
				}
			}
		]
	}
};

module.exports = config;