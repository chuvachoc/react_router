import React, { Component } from 'react';

import {
	BrowserRouter as Router,
	Route,
	Link,
	Switch
} from 'react-router-dom'


import Sites from './pages/sites';
import Templates from './pages/templates';
import Files from './pages/files';
import Account from './pages/account';


class Content extends Component {

	render() {
		return (
			<Router>
				<div>
					<ul>
						<li><Link to="/">Sites</Link></li>
						<li><Link to="/templates">Templates</Link></li>
						<li><Link to="/files">Files</Link></li>
						<li><Link to="/account">Account</Link></li>
					</ul>

					<Switch>
						<Route strict={false} exact path="/(cart|up|cart/qwe-asd|upgrade|upgrade/qwe)?" component={Sites}/>
						<Route path="/templates" component={Templates}/>
						<Route path="/files" component={Files}/>
						<Route path="/account" component={Account}/>
						<Route render={() => ( <div>404</div> )}/>
					</Switch>
				</div>
			</Router>
		);
	}
}

export default Content;