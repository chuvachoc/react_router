import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import {
	BrowserRouter as Router,
	Route,
	Link,
	Switch
} from 'react-router-dom'

import Modal from './pages/modal';
import Content from './Content';

class Main extends Component {

	render() {
		return (
			<div>
				<Router>
					<div>
						<ul>
							<li><Link to="/cart">Cart</Link></li>
							<li><Link to="/upgrade">Upgrade</Link></li>
						</ul>

						<Route path="/(cart|upgrade)?" component={Modal}/>
					</div>
				</Router>
				<Content />
			</div>
		)
	}
}

ReactDOM.render(
	<Main />,
	document.getElementById('root')
);
