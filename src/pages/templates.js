import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import {
	BrowserRouter as Router,
	Route,
	Link,
	NavLink
} from 'react-router-dom'



// children
class FaceView extends Component {
	render() {
		return (
			<h3>
				This is templates page
			</h3>
		)
	}
}

class Template1 extends Component {
	render() {
		return (
			<h3>
				About Template 1
			</h3>
		)
	}
}

class Template2 extends Component {
	render() {
		return (
			<h3>
				About Template 2
			</h3>
		)
	}
}
// end children




class Templates extends Component {


	render(){
		return (
			<div>
				<h1>
					Templates page
				</h1>

				<div>
					<ul>
						<li><NavLink activeClassName="active" to="/templates/temp1">Temp 1</NavLink></li>
						<li><NavLink activeClassName="active" to="/templates/temp2">Temp 2</NavLink></li>
					</ul>

					<Route exact path="/templates" component={FaceView}/>
					<Route path="/templates/temp1" component={Template1}/>
					<Route path="/templates/temp2" component={Template2}/>
				</div>
			</div>
		);
	}
}

export default Templates