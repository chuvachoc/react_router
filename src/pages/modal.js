import React, { Component } from 'react';
import { CSSTransitionGroup } from 'react-transition-group';


class Modal extends Component {
	render() {
		const {name} = this.props;

		return (<div>Hello modal {name}!!!</div>);
	}
}


class Card extends Component {

	render() {
		const { location: { pathname } } = this.props;

		const key = pathname && pathname.replace('/', '');

		return (
			<div>
				{ (key === 'cart' || key === 'upgrade') && (<Modal name={key} />) }
			</div>
		);
	}
}

export default Card