import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import {
	BrowserRouter as Router,
	Route,
	Link
} from 'react-router-dom'



// children
class FaceView extends Component {
	render() {
		return (
			<h3>
				This is files page
			</h3>
		)
	}
}

class List extends Component {

	render() {
		const { match: { params= {} } } = this.props;

		return (
			<div>
				Page { params.id }
			</div>
		)
	}
}
// end children

class Files extends Component {
	render(){
		return (
			<div>
				<h1>Files page</h1>


				<div>
					<ul>
						<li><Link to="/files/">Start page</Link></li>
						<li><Link to="/files/images">Images</Link></li>
						<li><Link to="/files/films">Films</Link></li>
						<li><Link to="/files/music">Music</Link></li>
					</ul>

					<Route exact path="/files/" component={FaceView}/>
					<Route path="/files/:id" component={List}/>
				</div>
			</div>
		);
	}
}

export default Files