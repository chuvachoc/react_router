import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import {
	BrowserRouter as Router,
	Route,
	Link
} from 'react-router-dom'



// children
class Account extends Component {
	render() {
		return (
			<h3>
				User data
			</h3>
		)
	}
}
class Subscriptions extends Component {
	render() {
		return (
			<h3>
				Profile subscriptions
			</h3>
		)
	}
}
class Mails extends Component {
	render() {
		const { match: { params= {} } } = this.props;

		return (
			<div>
				<h3>
					Mails list
				</h3>
				<div>
					<ul>
						<li><Link to="/account/mails/1">Mail 1</Link></li>
						<li><Link to="/account/mails/2">Mail 2</Link></li>
						<li><Link to="/account/mails/3">Mail 3</Link></li>
					</ul>
				</div>
				{
					params.id && (
						<div>
							<h5>
								Letter number { params.id }
							</h5>
						</div>
					)
				}
			</div>
		)
	}
}
// end children

class AccountWrap extends Component {
	render(){
		return (
			<div>
				<h1>Files page</h1>

				<div>
					<ul>
						<li><Link to="/account/">My Account</Link></li>
						<li><Link to="/account/subscriptions">Subscriptions</Link></li>
						<li><Link to="/account/mails">Mails</Link></li>
					</ul>

					<Route exact path="/account/" render={props => (<Account customProp="hello" />) } />
					<Route path="/account/subscriptions" component={Subscriptions}/>
					<Route path="/account/mails/:id?" component={Mails}/>
				</div>
			</div>
		);
	}
}

export default AccountWrap